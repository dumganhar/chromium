// Copyright 2016 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// https://w3c.github.io/remote-playback/#idl-def-remoteplayback

enum RemotePlaybackState {
    "connected",
    "disconnected"
};

callback RemotePlaybackAvailabilityCallback = void(boolean available);

[
    ActiveScriptWrappable,
    Custom=VisitDOMWrapper,
    DependentLifetime,
    RuntimeEnabled=RemotePlayback
] interface RemotePlayback : EventTarget {
    readonly attribute RemotePlaybackState state;
    attribute EventHandler onconnecting;
    attribute EventHandler onconnect;
    attribute EventHandler ondisconnect;

    Promise<long> watchAvailability(RemotePlaybackAvailabilityCallback callback);
    Promise<void> cancelWatchAvailability(optional long id);
    Promise<void> prompt();
};
